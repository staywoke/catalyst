FactoryGirl.define do
  factory :domain do
    name 'Domain'

    factory :cities do
      name 'Cities'
    end
  end
end
