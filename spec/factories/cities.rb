FactoryGirl.define do
  factory :city do
    name 'San Francisco'
    state 'California'
    latitude 37.768202
    longitude(-122.436922)
  end
end
