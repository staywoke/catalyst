# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161129182153) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "action_suggestion_responses", force: :cascade do |t|
    t.text     "description",  null: false
    t.text     "motivation"
    t.integer  "user_id",      null: false
    t.string   "project_key",  null: false
    t.string   "token"
    t.boolean  "correct"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "organization"
    t.index ["user_id"], name: "index_action_suggestion_responses_on_user_id", using: :btree
  end

  create_table "action_view_statistics", force: :cascade do |t|
    t.integer  "user_id",     null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "project_key", null: false
    t.integer  "city_id"
    t.index ["city_id"], name: "index_action_view_statistics_on_city_id", using: :btree
    t.index ["user_id"], name: "index_action_view_statistics_on_user_id", using: :btree
  end

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.string   "state"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "civilian_review_board_policy_responses", force: :cascade do |t|
    t.string   "name",             null: false
    t.string   "url",              null: false
    t.integer  "city_id"
    t.integer  "user_id",          null: false
    t.string   "project_key"
    t.boolean  "correct"
    t.string   "canonical_answer"
    t.string   "token"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["city_id"], name: "index_civilian_review_board_policy_responses_on_city_id", using: :btree
    t.index ["user_id"], name: "index_civilian_review_board_policy_responses_on_user_id", using: :btree
  end

  create_table "domain_memberships", force: :cascade do |t|
    t.integer  "domain_id"
    t.integer  "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_domain_memberships_on_city_id", using: :btree
    t.index ["domain_id"], name: "index_domain_memberships_on_domain_id", using: :btree
  end

  create_table "domains", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hate_incident_responses", force: :cascade do |t|
    t.date     "date",        null: false
    t.string   "location",    null: false
    t.text     "description", null: false
    t.integer  "user_id",     null: false
    t.string   "project_key", null: false
    t.string   "token"
    t.boolean  "correct"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "link"
    t.index ["user_id"], name: "index_hate_incident_responses_on_user_id", using: :btree
  end

  create_table "ip_addresses", force: :cascade do |t|
    t.string   "address",    null: false
    t.float    "latitude",   null: false
    t.float    "longitude",  null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "legacy_survey_response_statistics", force: :cascade do |t|
    t.integer  "legacy_survey_response_id"
    t.integer  "view_count",                default: 0, null: false
    t.datetime "redeemed_at"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.index ["legacy_survey_response_id"], name: "index_lsrs_on_legacy_survey_response_id", using: :btree
  end

  create_table "legacy_survey_responses", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "zip_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "token"
  end

  create_table "locations", force: :cascade do |t|
    t.string   "name",       null: false
    t.float    "latitude",   null: false
    t.float    "longitude",  null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "policy_proposal_responses", force: :cascade do |t|
    t.text     "proposal",    null: false
    t.text     "evidence",    null: false
    t.integer  "user_id",     null: false
    t.string   "project_key", null: false
    t.string   "token"
    t.boolean  "correct"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["user_id"], name: "index_policy_proposal_responses_on_user_id", using: :btree
  end

  create_table "project_domains", force: :cascade do |t|
    t.string   "project_key"
    t.integer  "domain_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["domain_id"], name: "index_project_domains_on_domain_id", using: :btree
  end

  create_table "protest_proposal_responses", force: :cascade do |t|
    t.text     "proposal",    null: false
    t.text     "motivation"
    t.integer  "user_id",     null: false
    t.string   "project_key", null: false
    t.string   "token"
    t.boolean  "correct"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["user_id"], name: "index_protest_proposal_responses_on_user_id", using: :btree
  end

  create_table "racism_curriculum_responses", force: :cascade do |t|
    t.text     "proposal",    null: false
    t.string   "audience",    null: false
    t.string   "link"
    t.integer  "user_id",     null: false
    t.string   "project_key", null: false
    t.string   "token"
    t.boolean  "correct"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["user_id"], name: "index_racism_curriculum_responses_on_user_id", using: :btree
  end

  create_table "response_statistics", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "correct",    default: 0, null: false
    t.integer  "incorrect",  default: 0, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["user_id"], name: "index_response_statistics_on_user_id", using: :btree
  end

  create_table "training_interest_responses", force: :cascade do |t|
    t.string   "domain",      null: false
    t.integer  "user_id",     null: false
    t.string   "project_key", null: false
    t.string   "token"
    t.boolean  "correct"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["user_id"], name: "index_training_interest_responses_on_user_id", using: :btree
  end

  create_table "trump_reasons_responses", force: :cascade do |t|
    t.string   "policy",      null: false
    t.text     "impact",      null: false
    t.integer  "user_id",     null: false
    t.string   "project_key", null: false
    t.string   "token"
    t.boolean  "correct"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["user_id"], name: "index_trump_reasons_responses_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "location"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.integer  "legacy_survey_response_id"
    t.string   "email",                     default: ""
    t.string   "encrypted_password",        default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",             default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.boolean  "admin",                     default: false, null: false
    t.float    "latitude"
    t.float    "longitude"
    t.string   "name"
    t.string   "phone"
    t.boolean  "is_organization",           default: false, null: false
    t.string   "external_uid_facebook"
    t.string   "organization_name"
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["external_uid_facebook"], name: "index_users_on_external_uid_facebook", using: :btree
    t.index ["legacy_survey_response_id"], name: "index_users_on_legacy_survey_response_id", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "action_view_statistics", "users"
  add_foreign_key "domain_memberships", "cities"
  add_foreign_key "domain_memberships", "domains"
  add_foreign_key "legacy_survey_response_statistics", "legacy_survey_responses"
  add_foreign_key "response_statistics", "users"
end
