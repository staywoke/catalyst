class AddLinkToHateIncidentResponses < ActiveRecord::Migration[5.0]
  def change
    add_column :hate_incident_responses, :link, :string
  end
end
