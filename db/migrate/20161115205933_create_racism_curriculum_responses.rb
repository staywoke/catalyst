class CreateRacismCurriculumResponses < ActiveRecord::Migration[5.0]
  def change
    create_table :racism_curriculum_responses do |t|
      t.text :proposal, null: false
      t.string :audience, null: false
      t.string :link

      t.references :user, null: false
      t.string :project_key, null: false

      t.string :token
      t.boolean :correct

      t.timestamps
    end
  end
end
