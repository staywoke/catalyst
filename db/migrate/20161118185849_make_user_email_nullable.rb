class MakeUserEmailNullable < ActiveRecord::Migration[5.0]
  def change
    change_column_null :users, :email, true
    remove_index :users, :email # remove the one with the uniqueness constraint
    add_index :users, :email
  end
end
