class AddFacebookUidToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :external_uid_facebook, :string
    add_index :users, :external_uid_facebook
  end
end
