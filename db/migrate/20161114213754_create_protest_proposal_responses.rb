class CreateProtestProposalResponses < ActiveRecord::Migration[5.0]
  def change
    create_table :protest_proposal_responses do |t|
      t.text :proposal, null: false
      t.text :motivation

      t.references :user, null: false
      t.string :project_key, null: false
      t.string :token
    end
  end
end
