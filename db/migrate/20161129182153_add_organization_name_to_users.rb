class AddOrganizationNameToUsers < ActiveRecord::Migration[5.0]
  def change
    rename_column :users, :organization, :is_organization
    add_column :users, :organization_name, :string
  end
end
