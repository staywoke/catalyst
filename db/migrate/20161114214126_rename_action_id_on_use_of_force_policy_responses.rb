class RenameActionIdOnUseOfForcePolicyResponses < ActiveRecord::Migration[5.0]
  def change
    Responses::UseOfForcePolicyResponse.destroy_all

    remove_reference :use_of_force_policy_responses, :action

    add_column :use_of_force_policy_responses, :project_key, :string
    add_reference :use_of_force_policy_responses, :city
  end
end
