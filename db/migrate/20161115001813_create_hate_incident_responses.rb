class CreateHateIncidentResponses < ActiveRecord::Migration[5.0]
  def change
    create_table :hate_incident_responses do |t|
      t.date :date, null: false
      t.string :location, null: false
      t.text :description, null: false

      t.references :user, null: false
      t.string :project_key, null: false

      t.string :token
      t.boolean :correct

      t.timestamps
    end
  end
end
