class ConsolidateUsersNames < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :name, :string

    User.all.each do |user|
      user.name = "#{user.first_name} #{user.last_name}"
      user.name.strip!
      user.save!
    end

    remove_column :users, :first_name, :string
    remove_column :users, :last_name, :string
  end
end
