class AddOrganizationToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :organization, :boolean, default: false, null: false
  end
end
