class RemoveUseOfForcePolicyResponses < ActiveRecord::Migration[5.0]
  def change
    drop_table :use_of_force_policy_responses
  end
end
