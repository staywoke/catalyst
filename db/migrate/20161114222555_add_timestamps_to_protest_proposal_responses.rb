class AddTimestampsToProtestProposalResponses < ActiveRecord::Migration[5.0]
  def change
    add_timestamps :protest_proposal_responses
  end
end
