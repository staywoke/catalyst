class CreateTrumpReasonsResponses < ActiveRecord::Migration[5.0]
  def change
    create_table :trump_reasons_responses do |t|
      t.string :policy, null: false
      t.text :impact, null: false

      t.references :user, null: false
      t.string :project_key, null: false

      t.string :token
      t.boolean :correct

      t.timestamps
    end
  end
end
