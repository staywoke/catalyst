class CreateTrainingInterestResponses < ActiveRecord::Migration[5.0]
  def change
    create_table :training_interest_responses do |t|
      t.string :domain, null: false

      t.references :user, null: false
      t.string :project_key, null: false

      t.string :token
      t.boolean :correct

      t.timestamps
    end
  end
end
