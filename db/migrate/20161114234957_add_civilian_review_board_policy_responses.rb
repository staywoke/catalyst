class AddCivilianReviewBoardPolicyResponses < ActiveRecord::Migration[5.0]
  def change
    create_table :civilian_review_board_policy_responses do |t|
      t.string :name, null: false
      t.string :url, null: false

      t.references :city
      t.references :user, null: false
      t.string :project_key

      t.boolean :correct
      t.string :canonical_answer
      t.string :token

      t.timestamps
    end
  end
end
