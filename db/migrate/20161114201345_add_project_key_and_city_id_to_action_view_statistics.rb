class AddProjectKeyAndCityIdToActionViewStatistics < ActiveRecord::Migration[5.0]
  def change
    ActionViewStatistic.destroy_all
    remove_reference :action_view_statistics, :action

    add_column :action_view_statistics, :project_key, :string, null: false
    add_reference :action_view_statistics, :city
  end
end
