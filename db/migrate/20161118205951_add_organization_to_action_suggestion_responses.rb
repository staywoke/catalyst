class AddOrganizationToActionSuggestionResponses < ActiveRecord::Migration[5.0]
  def change
    add_column :action_suggestion_responses, :organization, :string
  end
end
