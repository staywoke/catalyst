class AddCorrectToProtestProposalResponses < ActiveRecord::Migration[5.0]
  def change
    add_column :protest_proposal_responses, :correct, :boolean
  end
end
