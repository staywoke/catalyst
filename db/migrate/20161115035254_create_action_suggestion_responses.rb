class CreateActionSuggestionResponses < ActiveRecord::Migration[5.0]
  def change
    create_table :action_suggestion_responses do |t|
      t.text :description, null: false
      t.text :motivation

      t.references :user, null: false
      t.string :project_key, null: false

      t.string :token
      t.boolean :correct

      t.timestamps
    end
  end
end
