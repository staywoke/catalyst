class CreateIpAddresses < ActiveRecord::Migration[5.0]
  def change
    create_table :ip_addresses do |t|
      t.string :address, null: false

      t.float :latitude, null: false
      t.float :longitude, null: false
    end
  end
end
