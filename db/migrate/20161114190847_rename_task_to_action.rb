class RenameTaskToAction < ActiveRecord::Migration[5.0]
  def change
    rename_table :task_views, :action_views
    rename_column :action_views, :task_id, :action_id

    rename_table :tasks, :actions

    rename_column :use_of_force_policy_responses, :task_id, :action_id
  end
end
