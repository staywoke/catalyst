class RenameActionViewToActionViewStatistic < ActiveRecord::Migration[5.0]
  def change
    rename_table :action_views, :action_view_statistics
  end
end
