source 'https://rubygems.org'
ruby '2.3.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.0', '>= 5.0.0.1'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
gem 'haml'

gem 'sidekiq'
gem 'sinatra', '>= 2.0.0.beta2', require: false # dependency of sidekiq-web -- using beta version because of https://github.com/mperham/sidekiq/issues/2839

gem 'devise'
gem 'bootstrap-sass', '~> 3.3.6'
gem 'simple_form'

gem 'mailgun_rails', require: false

gem 'underscore-rails'
gem 'underscore-string-rails'

gem 'geokit'
gem 'geokit-rails'

gem 'open_uri_redirections'

# For Facebook login
gem 'omniauth'
gem 'omniauth-facebook'

gem 'envied', '~> 0.8.1', git: 'https://github.com/goodguide/envied', branch: 'tweaks'

# For sending email via amazon SES
gem 'aws-sdk-rails', require: false

# reporting errors:
gem 'airbrake', '~> 5.6', require: false

gem 'rack-canonical-host'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
  gem 'pry'
  gem 'factory_girl_rails'
  gem 'awesome_print'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console'
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :development, :test do
  gem 'rspec-rails', '~> 3.5'
end
