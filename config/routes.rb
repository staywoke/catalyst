require 'sidekiq/web'

Rails.application.routes.draw do
  if ENVied.LIVE
    root to: 'static#home'
  else
    get '/home', to: 'static#home', as: :root

    if (url = ENV['TEMPORARY_REDIRECT_ROOT_ROUTE'].presence)
      get '/', to: redirect(url, status: 302)
    else
      get '/', to: 'static#coming_soon', as: :coming_soon
    end
  end

  devise_for :users, controller: { sessions: 'users/sessions' }

  get '/auth/:provider/callback', to: 'omniauth#callback'
  get '/auth/failure', to: redirect('/')

  devise_scope :user do
    get 'sign_in', to: 'devise/sessions#new'
  end

  get 'welcome', to: 'welcome#new', as: :welcome
  get 'welcome/:token', to: 'welcome#new'

  get 'account', to: 'users#edit', as: :account
  put 'account', to: 'users#update'
  get 'users/new', to: 'users#new', as: :new_user
  post 'users', to: 'users#create', as: :users

  get 'actions', to: 'actions#index', as: :actions
  get 'action', to: 'actions#show', as: :action
  post 'action', to: 'responses#create', as: :response

  namespace :admin do
    get 'projects', to: 'projects#index', as: :projects
    get 'projects/:key/edit', to: 'projects#edit', as: :edit_project
    put 'projects/:key', to: 'projects#update', as: :project

    get 'responses', to: 'responses#index', as: :responses
    get 'responses/:token', to: 'responses#show', as: :response
    put 'responses/:token', to: 'responses#update'

    resources 'cities', except: [:edit, :update]
    resources 'domains', except: :show
    resources 'users', only: 'index'

    authenticate :user, -> (u) { u.admin } do
      mount Sidekiq::Web => '/sidekiq'
    end
  end
end
