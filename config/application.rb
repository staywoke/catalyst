require_relative 'boot'

require 'rails/all'
require 'digest/sha2'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

ENVied.require(*Rails.groups)

module Catalyst
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.autoload_paths += Dir["#{config.root}/lib/"]

    config.external_url = ENVied.EXTERNAL_URL

    config.action_mailer.perform_deliveries = ENVied.RAILS_ACTION_MAILER_DELIVERY_URL?
    if config.action_mailer.perform_deliveries
      am_url = ENVied.RAILS_ACTION_MAILER_DELIVERY_URL
      case am_url.scheme

      when 'smtp'
        config.action_mailer.delivery_method = :smtp
        config.action_mailer.smtp_settings = {
          address: am_url.host,
          port: am_url.port,
          authentication: :login,
          user_name: am_url.user ? CGI.unescape(am_url.user) : nil,
          password: am_url.password ? CGI.unescape(am_url.password) : nil,
          enable_starttls_auto: true,
          domain: am_url.path.split('/')[1].presence,
          # ignore TLS certificate validity outside of production
          openssl_verify_mode: ENVied.DEV_MODE ? OpenSSL::SSL::VERIFY_NONE : OpenSSL::SSL::VERIFY_PEER
        }

      when 'amazon-ses'
        require 'aws-sdk-rails'
        config.action_mailer.delivery_method = :aws_sdk
        # eagerly attempt to create an SES client, in order to validate that credentials are present
        Aws::SES::Client.new

      when 'mailgun'
        require 'mailgun_rails'
        config.action_mailer.delivery_method = :mailgun
        config.action_mailer.mailgun_settings = {
          api_key: ENV.fetch('MAILGUN_API_KEY'),
          domain: ENV.fetch('MAILGUN_DOMAIN'),
        }

      else
        fail "Don't know how to configure ActionMailer for RAILS_ACTION_MAILER_DELIVERY_URL=#{am_url.to_s.inspect}"
      end
    end

    config.action_mailer.default_options = {
      from: ENVied.RAILS_ACTION_MAILER_FROM,
      reply_to: ENVied.RAILS_ACTION_MAILER_REPLY_TO,
      'Message-ID' => -> (v) {
        "<#{Digest::SHA2.hexdigest(Time.now.to_i.to_s)}" \
        "@#{Rails.application.config.action_mailer.default_options[:from].split('@', 2).last}>"
      }
    }

    config.action_mailer.default_url_options = {
      host: config.external_url.host,
      protocol: config.external_url.scheme, # ugh rails... it's the "scheme", not the protocol
    }

    # only set port if non-standard for the URL scheme:
    case config.external_url.scheme
    when 'http'
      config.action_mailer.default_url_options[:port] = config.external_url.port if config.external_url.port != 80
    when 'https'
      config.action_mailer.default_url_options[:port] = config.external_url.port if config.external_url.port != 443
    else
      fail "Unknown EXTERNAL_URL scheme #{scheme}"
    end

    config.running_in_docker = File.exist?('/.dockerenv')

    config.log_level = ENV.fetch('LOG_LEVEL', 'info')

    # Prepend all log lines with the a per-request UUID (this helps tie multi-line log events together with each other and with events from a downstream Nginx, for example).
    config.log_tags = [:uuid]

    # Use a log formatter which both includes progname and correctly tags multi-line log events
    config.log_formatter = -> (severity, time, progname, msg) do
      line_prefix = severity[0, 1] << ' '
      line_prefix << "[#{progname}] " if progname.present?
      first_line, *lines = msg.split("\n")
      if (m = /\A(?:\[.+?\] )+/.match(first_line))
        line_prefix << m.to_s
        first_line = m.post_match
      end
      lines.inject(line_prefix + first_line << "\n") do |string, line|
        string << line_prefix << line << "\n"
      end
    end

    config.middleware.use Rack::CanonicalHost, config.external_url.host
  end
end
