class CalculateResponseStatisticsJob
  include Sidekiq::Worker

  def perform(id)
    user = User.find(id)

    # TODO: make hash tables for correct and incorrect
    # mapping action_ids to true/false values
    # At the end, map the "correct" and "incorrect"
    # to the true/false counts
    correct = Hash.new(0)
    incorrect = Hash.new(0)

    Responses::Base.reviewed_responses_for(user).each do |response|
      # TODO: `action_id` is no longer a thing. It's been replaced by
      # `project_key` and `city_id` (if there is a City). This code is broken.
      correct[response.action_id] = 1 if response.correct?
      incorrect[response.action_id] = 1 if response.incorrect?
    end

    statistics = user.statistics

    statistics.correct = correct.values.reduce(0, :+)
    statistics.incorrect = incorrect.values.reduce(0, :+)

    statistics.save!
  end
end
