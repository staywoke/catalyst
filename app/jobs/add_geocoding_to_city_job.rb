class AddGeocodingToCityJob
  include Sidekiq::Worker

  def perform(id)
    city = City.find(id)

    location = Location.location("#{city.name}, #{city.state}")

    city.update_column(:latitude, location.latitude)
    city.update_column(:longitude, location.longitude)
  end
end
