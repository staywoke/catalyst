class AddGeocodingToUserJob
  include Sidekiq::Worker

  def perform(id)
    user = User.find(id)

    return unless user.location.present?

    location = Location.location(user.location)

    user.update_column(:latitude, location.latitude)
    user.update_column(:longitude, location.longitude)
  end
end
