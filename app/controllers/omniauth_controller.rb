class OmniauthController < ApplicationController
  def callback
    @user = User.find_or_create_by_omniauth_hash(auth_hash)
    sign_in(:user, @user)
    redirect_to after_sign_in_path_for(:user)
  end

  protected

  def auth_hash
    request.env['omniauth.auth']
  end
end
