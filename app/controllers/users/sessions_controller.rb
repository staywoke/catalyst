class Users::SessionsController < Devise::SessionsController
  def create
    super
    session.delete(stored_location_key_for(:user))
  end

  def destroy
    super
    reset_session
  end
end
