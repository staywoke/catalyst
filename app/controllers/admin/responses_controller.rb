class Admin::ResponsesController < Admin::BaseController
  before_action :set_response, only: [:show, :update]

  def index
    @responses = Responses::Base.unreviewed
  end

  def update
    raise if params[:reject] && params[:accept]

    if params[:reject]
      @response.reject!(propagate: true)
      flash[:notice] = 'Response rejected'
    end

    if params[:accept]
      @response.accept!(propagate: true)
      flash[:notice] = 'Response accepted'
    end

    redirect_to admin_responses_path
  end

  private

  def set_response
    @response = Responses::Base.find_by_token(params[:token])
  end
end
