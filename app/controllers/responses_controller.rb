class ResponsesController < ApplicationController
  before_action :authenticate!

  def create
    @action = Action.find(city: params[:city_id], project: params[:project_key])

    @response = @action.new_response
    @response.assign_attributes(response_params)

    @response.user = current_user
    @response.city = @action.city if @action.city.present?
    @response.project_key = @action.project::KEY

    if @response.save
      @response.reload
      redirect_to actions_path(thanks: @response.token)
    else
      render 'actions/show'
    end
  end

  private

  def response_params
    params[response_params_key].permit(
      *@response.class::ALLOWED_ATTRIBUTES
    )
  end

  def response_params_key
    @response.class.name.sub('::', '').underscore
  end
end
