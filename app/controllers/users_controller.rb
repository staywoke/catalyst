class UsersController < ApplicationController
  before_action :authenticate!, only: [:edit, :update]
  before_action :set_user, only: [:edit, :update]

  def new
    @user = User.new
  end

  def create
    @user = User.new(new_user_params)

    if @user.save
      sign_in(:user, @user)
      redirect_to after_sign_in_path_for(:user)
    else
      case params[:user][:referrer]
      when 'welcome'
        render 'welcome/new'
      when 'homepage', 'sign-up'
        render 'users/new'
      else
        raise
      end
    end
  end

  def update
    old_password = edit_user_params[:old_password]
    new_password = edit_user_params[:new_password]

    edit_user_params.delete(:old_password)
    edit_user_params.delete(:new_password)

    @user.assign_attributes(edit_user_params)

    if @user.password_required? && ! @user.valid_password?(old_password)
      @user.errors.add(:old_password, 'was not correct')
      render :edit
      return
    end

    if new_password.present?
      @user.password = new_password
    else
      @user.password = old_password
    end

    if @user.save && sign_in(@user, :bypass => true)
      flash[:notice] = 'Your account has been successfully updated.'
      redirect_to actions_path
    else
      render :edit
    end
  end

  private

  def set_user
    @user = current_user
  end

  def edit_user_params
    @_edit_user_params ||= params.require(:user).permit(
      :email,
      :is_organization,
      :location,
      :name,
      :new_password,
      :old_password,
      :organization_name,
      :phone,
    )
  end

  def new_user_params
    @_new_user_params ||= params.require(:user).permit(
      :email,
      :is_organization,
      :legacy_survey_response_id,
      :location,
      :name,
      :organization_name,
      :password,
      :phone,
    )
  end
end
