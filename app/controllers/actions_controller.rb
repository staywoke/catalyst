require 'location_resolution_error'

class ActionsController < ApplicationController
  before_action :authenticate!, except: [:index]

  def index
    @actions = begin
      if params[:location].present?
        location = Location.location(params[:location])
        Action.most_relevant(location: location)
      elsif current_user
        Action.most_relevant(user: current_user)
      else
        location = IpAddress.location(request.remote_ip)
        Action.most_relevant(location: location)
      end
    rescue ::LocationResolutionError
      Action.most_relevant(location: Location.location('San Francisco, CA'))
    end

    if (token = params[:thanks].presence)
      last_response = Responses::Base.find_by_token(token)
      if last_response && last_response.created_at > 5.minutes.ago
        thanks = 'Thank you!'
        case @actions.count
        when 0
        when 1
          thanks += ' We have one more action we could use your help with:'
        when 2
          thanks += ' Here are a couple more actions we could use your help with:'
        else
          thanks += ' Here are a few more actions we could use your help with:'
        end
        flash.now[:notice] = thanks
      end
    end
  end

  def show
    @action = Action.find(city: params[:city_id], project: params[:project_key])
    @action.viewed_by!(current_user)

    @response = @action.new_response
  end
end
