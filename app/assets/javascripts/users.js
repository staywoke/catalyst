function organizationNameToggle() {
  if ($('#user_is_organization').prop('checked')) {
    $('#user_organization_name').slideDown();
  } else {
    $('#user_organization_name').slideUp();
    $('#user_organization_name').val('');
  }
}

$(document).on('turbolinks:load', function() {
  $('#user_is_organization').change(organizationNameToggle);
  organizationNameToggle();
});
