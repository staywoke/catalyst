class Location < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  validates :latitude, :longitude, presence: true

  class LocationResolutionError < ::LocationResolutionError
    def initialize(name, reason)
      @name = name
      @reason = reason
    end

    def message
      "Geolocation lookup failed for location `#{@name}`: #{@reason}"
    end
  end

  def self.location(name)
    result = self.find_by_name(name)
    return result if result.present?

    result = Geokit::Geocoders::MapboxGeocoder.geocode(name)

    fail LocationResolutionError.new(name, 'location is invalid') unless result.latitude.present? && result.longitude.present?

    Location.create!(
      name: name,
      latitude: result.latitude,
      longitude: result.longitude,
    )
  end
end
