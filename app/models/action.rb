class Action
  attr_accessor(
    :city,
    :project,
  )

  def self.find(project:, city: nil)
    result = Action.new

    result.project = Projects::Base.find_by_key(project)
    result.city = City.find(city) if city.present?

    result
  end

  def self.most_relevant(user: nil, location: nil, limit: 10)
    raise if user.nil? && location.nil?
    raise if user.present? && location.present?

    results = []

    origin = if location.present?
      [location.latitude, location.longitude]
    elsif user.present?
      [user.latitude, user.longitude]
    else
      raise
    end

    cities = City.by_distance(origin: origin).pluck(:id)

    projects = Projects::Base.descendants.sort do |x, y|
      x::PRIORITY <=> y::PRIORITY
    end

    projects.each do |project|
      next if project::META

      applicable_cities = if project < Projects::CityBased
        cities
      elsif project < Projects::Everyone
        [nil]
      else
        raise ApplicationError
      end

      applicable_cities.each do |city|
        action = Action.find(project: project::KEY, city: city)

        next if user && action.response_from?(user)
        next if action.enough_responses?

        results << action
        break
      end

      return results if results.count == limit
    end

    results
  end

  def viewed_by!(user)
    ActionViewStatistic.create!(
      user: user,
      project_key: self.project::KEY,
      city: self.city,
    )
  end

  def completers_count
    return 0 if project::URL.present?
    response_class.uniq(:user_id).count
  end

  def response_class
    project.response_class
  end

  def new_response
    project.new_response
  end

  def responses
    response_class.where(action: self).order(:id)
  end

  def title
    self.project::NAME
  end

  def subtitle
    return unless self.city
    "#{self.city.name}, #{self.city.state}"
  end

  def sponsor
    self.project::SPONSOR
  end

  def blurb
    self.project.blurb
  end

  def description
    self.project.description
  end

  def form
    self.project.form
  end

  def type
      if self.project::TYPE.present?
          return self.project::TYPE.gsub("_", " ").upcase
     end
  end

  def icon_inline
      if self.project::TYPE.present?
          return "actions/icons-inline/icons-inline-" + self.project::TYPE + ".png"
      end
  end

  def response_from?(user)
    return false if self.project::URL

    answers = response_class.where(project_key: self.project::KEY)
    answers = answers.where(city: self.city) if self.city.present?

    answers.where(user: user).count > 0
  end

  def enough_responses?
    return false if self.project < Projects::Everyone

    if self.project < Projects::CityBased
      count = response_class
        .where(project_key: self.project::KEY)
        .where(city: self.city)
        .where('correct IS NOT false')
        .count

      count >= self.project::MINIMUM_RESPONSES
    else
      raise ApplicationError
    end
  end
end
