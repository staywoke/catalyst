class IpAddress < ApplicationRecord
  validates :address, presence: true, uniqueness: true
  validates :latitude, :longitude, presence: true

  class LocationResolutionError < ::LocationResolutionError
    def initialize(ip_address, reason)
      @ip_address = ip_address
      @reason = reason
    end

    def message
      "IP address geolocation lookup failed for address #{@ip_address}: #{@reason}"
    end
  end

  def self.location(address)
    result = self.find_by_address(address)
    return result if result.present?

    resp = open("http://ipinfo.io/#{address}/geo")
    resp.status.first == '200' or fail LocationResolutionError.new(address, "response status = #{resp.status}")
    resp.content_type == 'application/json' or fail LocationResolutionError.new(address, "response not JSON")
    parsed = JSON.parse(resp.read)
    result = parsed.fetch('loc') {
      fail LocationResolutionError.new(address, "response object didn't include `loc` key: #{parsed.inspect}")
    }
    lat, lon = result.split(',',  2)
    fail LocationResolutionError.new(address, 'location is invalid') unless lat.present? && lon.present?

    IpAddress.create!(address: address, latitude: lat, longitude: lon)
  end
end
