class Projects::Base
  ABSTRACT = true
  META = false

  PRIORITY = nil
  URL = nil
  SPONSOR = nil

  def self.descendants
    [
      Projects::ActionSuggestionProject,
      Projects::AttendProtestProject,
      Projects::CivilianReviewBoardPolicyProject,
      Projects::HateIncidentProject,
      Projects::ImmigrationAssistanceProject,
      Projects::PolicyProposalProject,
      Projects::ProtestProposalProject,
      Projects::RacismCurriculumProject,
      Projects::TrainingInterestProject,
      Projects::TrumpReasonsProject,
    ]
  end

  def self.find_by_key(key)
    self.descendants.each do |klass|
      return klass if klass::KEY == key.to_sym
    end
  end

  def self.response_class
    name = self.name.split('::').last
    name.sub!(/Project$/, '')
    ('Responses::' + name + 'Response').constantize
  end

  def self.new_response
    self.response_class.new
  end

  def self.blurb
    "actions/#{self::KEY}/blurb"
  end

  def self.description
    "actions/#{self::KEY}/description"
  end

  def self.form
    "actions/#{self::KEY}/form"
  end

  def self.answer
    "actions/#{self::KEY}/answer"
  end
end
