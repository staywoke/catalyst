class Projects::CityBased < Projects::Base
  ABSTRACT = true
  MINIMUM_RESPONSES = 1

  def self.city_ids
    ::DomainMembership.
      where(domain: self.domains).
      where('city_id IS NOT NULL').
      pluck(:city_id).uniq.shuffle
  end

  def self.domains
    ::Domain.where(id: self.domain_ids)
  end

  def self.domain_ids
    ::ProjectDomain.where(project_key: self::KEY).pluck(:domain_id)
  end

  def self.domain_ids=(domain_ids)
    to_be_removed = self.domain_ids - domain_ids
    to_be_added = domain_ids - self.domain_ids

    to_be_removed.each do |domain_id|
      ::ProjectDomain.where(
        project_key: self::KEY,
        domain_id: domain_id
      ).destroy_all
    end

    to_be_added.each do |domain_id|
      ::ProjectDomain.create(project_key: self::KEY, domain_id: domain_id)
    end

    return if to_be_removed.empty? && to_be_added.empty?
  end
end
