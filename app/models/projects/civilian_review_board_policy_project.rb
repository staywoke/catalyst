class Projects::CivilianReviewBoardPolicyProject < Projects::CityBased
  MINIMUM_RESPONSES = 2
  KEY = :civilian_review_board_policy
  NAME = 'Find the Policy on Community Oversight of Police'
  PRIORITY = 107
  TYPE = 'policy'
end
