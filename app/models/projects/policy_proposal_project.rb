class Projects::PolicyProposalProject < Projects::Everyone
  KEY = :policy_proposal
  NAME = 'Propose a Policy Solution to End Racism'
  PRIORITY = 106
  TYPE = 'policy'
end
