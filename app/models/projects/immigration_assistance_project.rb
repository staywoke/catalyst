class Projects::ImmigrationAssistanceProject < Projects::Everyone
  KEY = :immigration_assistance
  NAME = 'Help Find Resources for People Faced with Immigration Issues'
  PRIORITY = 107
  URL = 'https://t.co/JUnQnl8JOW'
  TYPE = 'data'
  SPONSOR = 'The Frisco 5'
end
