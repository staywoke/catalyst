class Projects::TrumpReasonsProject < Projects::Everyone
  KEY = :trump_reasons
  NAME = "Tell Us How Trump's Agenda Will Impact Your Community"
  PRIORITY = 105
  TYPE = 'policy'
end
