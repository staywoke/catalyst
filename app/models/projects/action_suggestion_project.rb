class Projects::ActionSuggestionProject < Projects::Everyone
  KEY = :action_suggestion
  NAME = 'Submit a New Action/Task'
  PRIORITY = 103
  META = true
end
