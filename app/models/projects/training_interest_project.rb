class Projects::TrainingInterestProject < Projects::Everyone
  KEY = :training_interest
  NAME = 'Participate in a Training'
  PRIORITY = 104
  TYPE = 'training'
end
