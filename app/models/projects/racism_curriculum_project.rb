class Projects::RacismCurriculumProject < Projects::Everyone
  KEY = :racism_curriculum
  NAME = 'Design a Curriculum for Teaching Others about Racism'
  PRIORITY = 108
  TYPE = 'education'
end
