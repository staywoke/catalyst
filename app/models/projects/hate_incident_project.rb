class Projects::HateIncidentProject < Projects::Everyone
  KEY = :hate_incident
  NAME = 'Report an Incident of Hate'
  PRIORITY = 102
  TYPE = 'data'
end
