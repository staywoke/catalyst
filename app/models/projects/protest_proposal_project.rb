class Projects::ProtestProposalProject < Projects::Everyone
  KEY = :protest_proposal
  NAME = 'Propose an Idea for a Protest'
  PRIORITY = 101
  TYPE = 'direct_action'
end
