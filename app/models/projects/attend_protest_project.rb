class Projects::AttendProtestProject < Projects::Everyone
  KEY = :attend_protest
  NAME = 'Attend a Protest'
  PRIORITY = 100
  URL = 'http://m.dailykos.com/story/2016/11/15/1599582/-Trump-protests-calendar-and-information'
  TYPE = 'direct_action'
end
