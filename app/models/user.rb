class User < ApplicationRecord
  belongs_to :legacy_survey_response, optional: true

  devise :database_authenticatable, :recoverable, :rememberable, :trackable,
    :validatable

  validates :email, uniqueness: true, allow_blank: true
  validates :external_uid_facebook, uniqueness: true, allow_blank: true
  validate :has_email_or_external_uid

  before_create do
    if legacy_survey_response_id.present?
      object = LegacySurveyResponse.find(legacy_survey_response_id)
      object.statistics.redeemed!
    end
  end

  before_validation do
    begin
      populate_location_columns
    rescue ::LocationResolutionError => ex
      errors.add(:location, ex.message)
    end
  end

  def inflate_from_legacy_survey_response(legacy_survey_response)
    self.name = legacy_survey_response.name
    self.email = legacy_survey_response.email
    self.location = legacy_survey_response.zip_code

    self.legacy_survey_response_id = legacy_survey_response.id
  end

  def statistics
    ResponseStatistics.where(user: self).first_or_create
  end

  def self.find_or_create_by_omniauth_hash(auth_hash)
    id_column = ['external_uid', auth_hash.provider].join('_')
    where(id_column => auth_hash.uid).first_or_create!(
      name: auth_hash.info.name,
    )
  end

  def has_email_or_external_uid
    return if is_external?
    return if email.present?
    errors.add(:email, 'must be present')
  end

  def is_external?
    external_uid_facebook.present?
  end

  def email_required?
    ! is_external?
  end

  def password_required?
    ! is_external?
  end

  def populate_location_columns
    return unless location.present?

    result = Location.location(location)

    self.latitude = result.latitude
    self.longitude = result.longitude
  end
end
