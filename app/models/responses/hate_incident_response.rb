class Responses::HateIncidentResponse < Responses::Everyone
  ALLOWED_ATTRIBUTES = [
    :date,
    :description,
    :link,
    :location,
  ]

  validates :date, :description, :location, presence: true

  def answer
    description
  end
end
