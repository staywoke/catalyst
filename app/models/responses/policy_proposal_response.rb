class Responses::PolicyProposalResponse < Responses::Everyone
  ALLOWED_ATTRIBUTES = [
    :proposal,
    :evidence,
  ]

  validates :proposal, presence: true

  def answer
    proposal
  end
end
