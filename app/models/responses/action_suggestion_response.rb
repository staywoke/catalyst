class Responses::ActionSuggestionResponse < Responses::Everyone
  ALLOWED_ATTRIBUTES = [
    :description,
    :motivation,
    :organization,
  ]

  validates :description, presence: true

  def answer
    description
  end
end
