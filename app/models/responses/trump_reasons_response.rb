class Responses::TrumpReasonsResponse < Responses::Everyone
  ALLOWED_ATTRIBUTES = [
    :policy,
    :impact,
  ]

  validates :policy, :impact, presence: true

  def answer
    impact
  end
end
