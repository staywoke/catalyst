class Responses::CityBased < Responses::Base
  self.abstract_class = true
  belongs_to :city
end
