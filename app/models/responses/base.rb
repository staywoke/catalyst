class Responses::Base < ApplicationRecord
  self.abstract_class = true

  validates :project_key, presence: true

  belongs_to :user

  before_save do |record|
    record.token = SecureRandom.uuid unless token.present?
  end

  after_commit do |record|
    ::CalculateCanonicalAnswerJob.perform_async(record.class.name, record.id)
  end

  def self.descendants
    [
      Responses::ActionSuggestionResponse,
      Responses::CivilianReviewBoardPolicyResponse,
      Responses::HateIncidentResponse,
      Responses::PolicyProposalResponse,
      Responses::ProtestProposalResponse,
      Responses::RacismCurriculumResponse,
      Responses::TrainingInterestResponse,
      Responses::TrumpReasonsResponse,
    ]
  end

  def self.unreviewed
    result = []

    descendants.each do |klass|
      result += klass.where('correct IS NULL')
    end

    result
  end

  def self.find_by_token(token)
    descendants.each do |klass|
      result = klass.where(token: token).first
      return result if result
    end

    nil
  end

  def self.response_count_for(city)
    result = 0

    descendants.each do |klass|
      next if klass < Responses::Everyone
      result += klass.where(city: city).count
    end

    result
  end

  def self.reviewed_responses_for(user)
    result = []

    descendants.each do |klass|
      result += klass.where('correct IS NOT NULL').where(user: user)
    end

    result
  end

  def project
    Projects::Base.find_by_key(project_key)
  end

  def city_name
    try(:city).try(:name)
  end

  def incorrect?
    correct == false
  end

  def calculate_canonical_answer!
    raise NotImplementedError
  end

  def answer
    raise NotImplementedError
  end

  def accept!(propagate:)
    update_column(:correct, true)
    ::CalculateResponseStatisticsJob.perform_async(user.id)

    ::PropagateResponseJob.perform_async(self.class.name, id) if propagate
  end

  def reject!(propagate:)
    update_column(:correct, false)
    ::CalculateResponseStatisticsJob.perform_async(user.id)

    ::PropagateResponseJob.perform_async(self.class.name, id) if propagate
  end
end
