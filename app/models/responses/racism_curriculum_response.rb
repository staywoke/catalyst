class Responses::RacismCurriculumResponse < Responses::Everyone
  ALLOWED_ATTRIBUTES = [
    :proposal,
    :audience,
    :link,
  ]

  AUDIENCES = {
    'youth' => 'K-12',
    'college' => 'undergraduate',
    'adult' => 'adult',
  }

  validates :proposal, :audience, presence: true
  validates :audience, inclusion: {in: AUDIENCES.keys}

  def answer
    proposal
  end
end
