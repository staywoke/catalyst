class Responses::TrainingInterestResponse < Responses::Everyone
  ALLOWED_ATTRIBUTES = [
    :domain,
  ]

  DOMAINS = {
    'organizing' => 'Organizing/Direct Action',
    'research' => 'Policy Research',
    'advocacy' => 'Advocacy/Lobbying',
    'racism' => 'Anti-Racism (for White Allies)',
    'liberalism' => 'Understanding Neoliberalism, Liberalism, and Other Topics',
    'alliances' => 'Building Intergenerational Alliances',
    'youth' => 'Social Justice (for Youth)',
  }

  validates :domain, presence: true, inclusion: {in: DOMAINS.keys}

  def answer
    domain
  end
end
