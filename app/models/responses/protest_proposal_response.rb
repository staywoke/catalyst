class Responses::ProtestProposalResponse < Responses::Everyone
  ALLOWED_ATTRIBUTES = [
    :proposal,
    :motivation,
  ]

  validates :proposal, presence: true

  def answer
    proposal
  end
end
