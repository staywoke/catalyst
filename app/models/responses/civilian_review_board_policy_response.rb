class Responses::CivilianReviewBoardPolicyResponse < Responses::CityBased
  ALLOWED_ATTRIBUTES = [
    :name,
    :url,
  ]

  validates :name, :url, presence: true

  def calculate_canonical_answer!
    digest = Digest::MD5.hexdigest(
      open(self.url, allow_redirections: :safe).read
    )

    answer = "#{name}::#{digest}"

    self.update_column(:canonical_answer, answer)

    if user.admin? && self.correct.nil?
      self.accept!(propagate: true)
    end
  end

  def answer
    "#{name} #{url}"
  end
end
